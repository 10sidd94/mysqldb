cd /var/lib/jenkins/workspace/mysqldb

scp * ubuntu@ubuntu-20:/home/ubuntu/docker/CICD/bookproject-mysqldb

ssh ubuntu@ubuntu-20 "cd /home/ubuntu/docker/CICD/bookproject-mysqldb && docker build -t sql-image ."

ssh ubuntu@ubuntu-20 "docker rm -f sql-server"
ssh ubuntu@ubuntu-20 "docker run -it --rm --name sql-server --net bridge -e MYSQL_ROOT_PASSWORD=Hello@1sid -p 3306:3306 -d sql-image"


