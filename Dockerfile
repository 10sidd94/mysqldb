FROM mysql:8
ENV MYSQL_ROOT_PASSWORD Hello@1sid
ENV MYSQL_DATABASE sunbeam_db
ADD backup.sql /docker-entrypoint-initdb.d
EXPOSE 3306
